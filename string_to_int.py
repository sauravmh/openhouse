def stringToNumber(n: str) -> int:
  """
  Convert string to integer supports only integer strings and not float.

  Parameters:
    n (str): Str value to be converted
  
  Returns:
    int: Returns int value of given argument
  """

  num = 0

  # Handle negative values
  sign = '+'
  if n[0] == '-':
    sign = '-'
    n = n[1:]
  
  power = len(n) - 1 

  # uses ascii values to get the numerical values
  for i in n:
    diff = ord(i) - ord('0')

    # Handling when non-digit strings present
    if diff not in [0,1,2,3,4,5,6,7,8,9]:
      break
    
    num += diff * (10**power)
    power -= 1

  if sign == '-':
    return (num//(10**(power+1))) * -1

  return num//(10**(power+1))

for _ in range(int(input())):
  n = input()
  ans = stringToNumber(n)

  # Cross verification for value and value type for easy debugging
  print(ans, type(ans))
